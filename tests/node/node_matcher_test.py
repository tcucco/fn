# pylint: disable=too-many-public-methods, missing-docstring
from decimal import Decimal
from unittest import TestCase
from typing import (List)
import app.node.node_matcher as M
import app.node.node as N
from app.token import TokenList
from app.lexer import Lexer

def get_tokens(source: List[str]) -> TokenList:
  lexer = Lexer(source)
  return TokenList(list(lexer.process()))

class NodeMatcherTest(TestCase):
  def test_match_assignment(self):
    tokens = get_tokens(['(d, m) = (divmod x y)'])
    result = M.match_assignment(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.Assignment)
    self.assertEqual(node.length, 11)

    self.assertIsInstance(node.target, N.TupleAssignment)
    self.assertIsInstance(node.target.targets[0], N.Identifier)
    self.assertEqual(node.target.targets[0].name, 'd')
    self.assertIsInstance(node.target.targets[1], N.Identifier)
    self.assertEqual(node.target.targets[1].name, 'm')

    self.assertIsInstance(node.value, N.FunctionCall)
    self.assertIsInstance(node.value.function, N.Identifier)
    self.assertEqual(node.value.function.name, 'divmod')
    self.assertEqual(len(node.value.arguments), 2)
    self.assertIsInstance(node.value.arguments[0], N.Identifier)
    self.assertEqual(node.value.arguments[0].name, 'x')
    self.assertIsInstance(node.value.arguments[1], N.Identifier)
    self.assertEqual(node.value.arguments[1].name, 'y')

    self.assertFalse(tokens.has_more)

  def test_match_block(self):
    tokens = get_tokens([
      '{',
      '  sideA = (dist a b)',
      '  sideB = (dist a c)',
      '  (sqrt (add (sq a) (sq b)))',
      '}',
    ])
    result = M.match_block(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.Block)
    self.assertEqual(node.length, 30)

    self.assertEqual(len(node.assignments), 2)
    self.assertIsInstance(node.assignments[0], N.Assignment)
    self.assertIsInstance(node.assignments[1], N.Assignment)
    self.assertIsInstance(node.return_value, N.FunctionCall)

    self.assertFalse(tokens.has_more)

  def test_match_boolean(self):
    tokens = get_tokens(['true'])
    result = M.match_boolean(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.Boolean)
    self.assertEqual(node.value, True)

    self.assertFalse(tokens.has_more)

  def test_match_character(self):
    tokens = get_tokens(["'&'"])
    result = M.match_character(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.Character)
    self.assertEqual(node.value, '&')

    self.assertFalse(tokens.has_more)

  def test_match_discard(self):
    tokens = get_tokens(['_'])
    result = M.match_discard(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.Discard)
    self.assertEqual(node.length, 1)

    self.assertFalse(tokens.has_more)

  def test_match_function_call(self):
    tokens = get_tokens(['(add x 42)'])
    result = M.match_function_call(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.FunctionCall)
    self.assertEqual(node.length, 5)

    self.assertIsInstance(node.function, N.Identifier)
    self.assertEqual(node.function.name, 'add')
    self.assertEqual(len(node.arguments), 2)
    self.assertIsInstance(node.arguments[0], N.Identifier)
    self.assertEqual(node.arguments[0].name, 'x')
    self.assertIsInstance(node.arguments[1], N.Integer)
    self.assertEqual(node.arguments[1].value, 42)

    self.assertFalse(tokens.has_more)

  def test_match_function_call_2(self):
    tokens = get_tokens(['(always_bool)'])
    result = M.match_function_call(tokens)

    self.assertTrue(result.success)

    node = result.first

    self.assertIsInstance(node, N.FunctionCall)
    self.assertEqual(node.function.name, 'always_bool')
    self.assertEqual(node.arguments, [])

    self.assertFalse(tokens.has_more)

  def test_match_function_definition(self):
    tokens = get_tokens([
      '<A, B>((A) => B mapper, [A] input) => [B]',
      '  (if (empty input)',
      '      input',
      '      {'
      '        next = (mapper (head input))',
      '        rest = (tail input)',
      '        (prepend next rest)',
      '      }',
      '  )',
    ])
    result = M.match_function_definition(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.FunctionDefinition)

    self.assertEqual(len(node.type_variables), 2)
    self.assertEqual(node.type_variables[0].name, 'A')
    self.assertEqual(node.type_variables[1].name, 'B')

    self.assertEqual(len(node.arguments), 2)
    self.assertIsInstance(node.arguments[0], N.FunctionDefinitionArgument)
    self.assertIsInstance(node.arguments[0].type, N.FunctionType)
    self.assertIsInstance(node.arguments[1], N.FunctionDefinitionArgument)
    self.assertIsInstance(node.arguments[1].type, N.ListType)
    self.assertIsInstance(node.return_type, N.ListType)

    self.assertIsInstance(node.body, N.FunctionCall)
    self.assertIsInstance(node.body.function, N.Identifier)
    self.assertEqual(node.body.function.name, 'if')
    self.assertEqual(len(node.body.arguments), 3)
    self.assertIsInstance(node.body.arguments[0], N.FunctionCall)
    self.assertIsInstance(node.body.arguments[1], N.Identifier)
    self.assertIsInstance(node.body.arguments[2], N.Block)

    self.assertFalse(tokens.has_more)

  def test_match_function_definition_2(self):
    # A function that returns a constant value
    tokens = get_tokens(['() => Boolean true'])
    result = M.match_function_definition(tokens)

    self.assertTrue(result.success)
    self.assertFalse(tokens.has_more)

  def test_match_function_definition_3(self):
    # A function that takes a value and returns a function that always returns that value
    tokens = get_tokens(['<A>(A a) => () => A () => A a'])
    result = M.match_function_definition(tokens)

    self.assertTrue(result.success)

    node = result.first

    self.assertIsInstance(node.return_type, N.FunctionType)
    self.assertIsInstance(node.body, N.FunctionDefinition)
    self.assertIsInstance(node.body.body, N.Identifier)
    self.assertEqual(node.body.body.name, 'a')

    self.assertFalse(tokens.has_more)

  def test_match_function_definition_4(self):
    # A function that takes a value and returns a function that always returns that value
    tokens = get_tokens(['(Int a, Int b) => (add (mult a 2) (mult b 2))'])
    result = M.match_function_definition(tokens)

    self.assertTrue(result.success)

    node = result.first

    self.assertIsNone(node.return_type)
    self.assertIsInstance(node.body, N.FunctionCall)
    self.assertEqual(node.body.function.name, 'add')

    self.assertFalse(tokens.has_more)

  def test_match_function_definition_5(self):
    # A function that returns a constant value
    tokens = get_tokens(['() => true'])
    result = M.match_function_definition(tokens)

    self.assertTrue(result.success)

    node = result.first

    self.assertIsNone(node.return_type)
    self.assertIsInstance(node.body, N.Boolean)

    self.assertFalse(tokens.has_more)

  def test_match_function_definition_argument(self):
    tokens = get_tokens(['Int x'])
    result = M.match_function_definition_argument(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.FunctionDefinitionArgument)
    self.assertEqual(node.length, 2)
    self.assertIsInstance(node.type, N.TypeName)
    self.assertEqual(node.type.name, 'Int')
    self.assertIsInstance(node.name, N.Identifier)
    self.assertEqual(node.name.name, 'x')

    self.assertFalse(tokens.has_more)

  def test_match_function_definition_arguments(self):
    tokens = get_tokens(['(Int x, Int y)'])
    result = M.match_function_definition_arguments(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 2)

    node1 = result.nodes[0]

    self.assertIsInstance(node1, N.FunctionDefinitionArgument)
    self.assertEqual(node1.length, 2)
    self.assertIsInstance(node1.type, N.TypeName)
    self.assertEqual(node1.type.name, 'Int')
    self.assertIsInstance(node1.name, N.Identifier)
    self.assertEqual(node1.name.name, 'x')

    node2 = result.nodes[1]

    self.assertIsInstance(node2, N.FunctionDefinitionArgument)
    self.assertEqual(node2.length, 2)
    self.assertIsInstance(node2.type, N.TypeName)
    self.assertEqual(node2.type.name, 'Int')
    self.assertIsInstance(node2.name, N.Identifier)
    self.assertEqual(node2.name.name, 'y')

    self.assertFalse(tokens.has_more)

  def test_match_function_type(self):
    tokens = get_tokens(['(Int, Int) => Real'])
    result = M.match_function_type(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.FunctionType)
    self.assertEqual(len(node.arguments), 2)
    self.assertEqual(node.arguments[0].name, 'Int')
    self.assertEqual(node.arguments[1].name, 'Int')
    self.assertEqual(node.return_type.name, 'Real')

    self.assertFalse(tokens.has_more)

  def test_match_function_type_1_arg(self):
    tokens = get_tokens(['(Int) => Real'])
    result = M.match_function_type(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.FunctionType)
    self.assertEqual(len(node.arguments), 1)
    self.assertEqual(node.arguments[0].name, 'Int')
    self.assertEqual(node.return_type.name, 'Real')

    self.assertFalse(tokens.has_more)

  def test_match_function_type_0_arg(self):
    tokens = get_tokens(['() => Real'])
    result = M.match_function_type(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.FunctionType)
    self.assertEqual(len(node.arguments), 0)
    self.assertEqual(node.return_type.name, 'Real')

    self.assertFalse(tokens.has_more)

  def test_match_identifier(self):
    tokens = get_tokens(['circleRadius'])
    result = M.match_identifier(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.Identifier)
    self.assertEqual(node.name, 'circleRadius')

    self.assertFalse(tokens.has_more)

  def test_match_integer(self):
    tokens = get_tokens(['9042'])
    result = M.match_integer(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.Integer)
    self.assertEqual(node.value, 9042)

    self.assertFalse(tokens.has_more)

  def test_match_list(self):
    tokens = get_tokens(['[1, 2, 3, 4, 5]'])
    result = M.match_list(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.List)
    self.assertEqual(node.length, 11)
    self.assertEqual(len(node.values), 5)
    self.assertIsInstance(node.values[0], N.Integer)
    self.assertEqual(node.values[0].value, 1)
    self.assertIsInstance(node.values[1], N.Integer)
    self.assertEqual(node.values[1].value, 2)
    self.assertIsInstance(node.values[2], N.Integer)
    self.assertEqual(node.values[2].value, 3)
    self.assertIsInstance(node.values[3], N.Integer)
    self.assertEqual(node.values[3].value, 4)
    self.assertIsInstance(node.values[4], N.Integer)
    self.assertEqual(node.values[4].value, 5)

    self.assertFalse(tokens.has_more)

  def test_match_list_type(self):
    tokens = get_tokens(['[Real]'])
    result = M.match_list_type(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.ListType)
    self.assertEqual(node.length, 3)
    self.assertIsInstance(node.member_type, N.TypeName)
    self.assertEqual(node.member_type.name, 'Real')
    self.assertEqual(tokens.index, 3)

    self.assertFalse(tokens.has_more)

  def test_match_program(self):
    tokens = get_tokens([
      'isEmpty = <A>([A] xs) => (eq 0 (length xs))',
      '',
      'headTail = <A>([A] xs) => ((head xs), (tail xs))',
      '',
      'zip = <A, B>([A] xs, [B] ys) => [(A, B)] {',
      '  (headA, tailA) = (headTail xs)',
      '  (headB, tailB) = (headTail ys)',
      '  next = (headA, headB)',
      '  (if (or (isEmpty tailA) (isEmpty tailB))',
      '    [next]',
      '    (prepend next (zip tailA tailB))',
      '  )',
      '}',
    ])
    result = M.match_program(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.Program)
    self.assertEqual(node.assignments[0].target.name, 'isEmpty')
    self.assertEqual(node.assignments[1].target.name, 'headTail')
    self.assertEqual(node.assignments[2].target.name, 'zip')
    self.assertEqual(len(node.assignments), 3)

    self.assertFalse(tokens.has_more)

  def test_match_real(self):
    tokens = get_tokens(['42.43'])
    result = M.match_real(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.Real)
    self.assertEqual(node.value, Decimal('42.43'))

    self.assertFalse(tokens.has_more)

  def test_match_string(self):
    tokens = get_tokens(['"hello, world!"'])
    result = M.match_string(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.String)
    self.assertEqual(node.value, 'hello, world!')

    self.assertFalse(tokens.has_more)

  def test_match_tuple(self):
    tokens = get_tokens(['("Trey Cucco", 35, 5.11)'])
    result = M.match_tuple(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.Tuple)
    self.assertEqual(node.length, 7)
    self.assertEqual(len(node.values), 3)
    self.assertIsInstance(node.values[0], N.String)
    self.assertEqual(node.values[0].value, 'Trey Cucco')
    self.assertIsInstance(node.values[1], N.Integer)
    self.assertEqual(node.values[1].value, 35)
    self.assertIsInstance(node.values[2], N.Real)
    self.assertEqual(node.values[2].value, Decimal('5.11'))

    self.assertFalse(tokens.has_more)

  def test_match_tuple_assignment(self):
    tokens = get_tokens(['((x1, y1), _, (x2, y2))'])
    result = M.match_tuple_assignment(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.TupleAssignment)
    self.assertEqual(node.length, 15)
    (tup1, dis, tup2) = node.targets

    self.assertIsInstance(tup1, N.TupleAssignment)
    self.assertIsInstance(tup1.targets[0], N.Identifier)
    self.assertEqual(tup1.targets[0].name, 'x1')
    self.assertIsInstance(tup1.targets[1], N.Identifier)
    self.assertEqual(tup1.targets[1].name, 'y1')

    self.assertIsInstance(dis, N.Discard)

    self.assertIsInstance(tup2, N.TupleAssignment)
    self.assertIsInstance(tup2.targets[0], N.Identifier)
    self.assertEqual(tup2.targets[0].name, 'x2')
    self.assertIsInstance(tup2.targets[1], N.Identifier)
    self.assertEqual(tup2.targets[1].name, 'y2')

    self.assertFalse(tokens.has_more)

  def test_match_tuple_type(self):
    tokens = get_tokens(['(String, Int)'])
    result = M.match_tuple_type(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.TupleType)
    self.assertEqual(node.length, 5)
    self.assertEqual(len(node.member_types), 2)
    self.assertEqual(node.member_types[0].name, 'String')
    self.assertEqual(node.member_types[1].name, 'Int')
    self.assertEqual(tokens.index, 5)

    self.assertFalse(tokens.has_more)

  def test_match_type_name(self):
    tokens = get_tokens(['Int'])
    result = M.match_type_name(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.TypeName)
    self.assertEqual(node.name, 'Int')
    self.assertEqual(tokens.index, 1)

    self.assertFalse(tokens.has_more)

  def test_match_type_variables(self):
    tokens = get_tokens(['<A, B>'])
    result = M.match_type_variables(tokens)

    self.assertTrue(result.success)
    self.assertEqual(len(result.nodes), 1)

    node = result.first

    self.assertIsInstance(node, N.TypeVariables)
    self.assertEqual(node.length, 5)
    self.assertIsInstance(node.type_names[0], N.TypeName)
    self.assertEqual(node.type_names[0].name, 'A')
    self.assertIsInstance(node.type_names[1], N.TypeName)
    self.assertEqual(node.type_names[1].name, 'B')

    self.assertFalse(tokens.has_more)
