"""Contains classes and methods for working with scope."""
from .scope import (
  Function,
  Scope,
  Value,
)
