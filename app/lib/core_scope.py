"""Defines the core scope."""
from typing import (
  Callable,
  Sequence,
)
import app.lib.core as c
from app.scope.scope import Scope
from app.scope.key import (
  TypeKey,
  TypeVariableKey,
  ContainerTypeKey,
  FunctionScopeKey,
)
from app.lib.types import (
  int_type,
  real_type,
  bool_type,
  char_type,
  list_type,
  function_type,
)

Int = TypeKey(int_type)
Real = TypeKey(real_type)
Bool = TypeKey(bool_type)
Char = TypeKey(char_type)
Var = TypeVariableKey('A')
List = ContainerTypeKey(list_type, [Var])
Pred = ContainerTypeKey(function_type, [Bool])
Thunk = ContainerTypeKey(function_type, [Var])

def _register(
    scope: Scope,
    name: str,
    arg_types: Sequence[TypeKey],
    return_type: TypeKey,
    fn: Callable
  ):
  scope.add(FunctionScopeKey(name, arg_types, return_type), fn)

def build_core_scope():
  """Builds a core Scope instance."""
  scope = Scope()

  # integer
  _register(scope, 'add', [Int, Int], Int, c.int_add)
  _register(scope, 'sub', [Int, Int], Int, c.int_subtract)
  _register(scope, 'mul', [Int, Int], Int, c.int_multiply)
  _register(scope, 'div', [Int, Int], Int, c.int_divide)
  _register(scope, 'lt', [Int, Int], Bool, c.int_less_than)
  _register(scope, 'eq', [Int, Int], Bool, c.int_equal)

  # real
  _register(scope, 'add', [Real, Real], Real, c.real_add)
  _register(scope, 'sub', [Real, Real], Real, c.real_subtract)
  _register(scope, 'mul', [Real, Real], Real, c.real_multiply)
  _register(scope, 'div', [Real, Real], Real, c.real_divide)
  _register(scope, 'lt', [Real, Real], Bool, c.real_less_than)
  _register(scope, 'eq', [Real, Real], Bool, c.real_equal)

  # boolean
  _register(scope, 'eq', [Bool, Bool], Bool, c.bool_equal)
  _register(scope, 'or', [Bool, Bool], Bool, c.bool_or)
  _register(scope, 'or', [Pred, Pred], Bool, c.bool_or_fn)
  _register(scope, 'and', [Bool, Bool], Bool, c.bool_and)
  _register(scope, 'and', [Pred, Pred], Bool, c.bool_and_fn)
  _register(scope, 'if', [Bool, Var, Var], Var, c.bool_if)
  _register(scope, 'iff', [Bool, Thunk, Thunk], Var, c.bool_if_fn)

  # character
  _register(scope, 'lt', [Char, Char], Bool, c.char_less_than)
  _register(scope, 'eq', [Char, Char], Bool, c.char_equal)

  # list
  _register(scope, 'len', [List], Int, c.list_length)
  _register(scope, 'at', [Int, List], Var, c.list_item)
  _register(scope, 'slice', [Int, Int, List], List, c.list_slice)
  _register(scope, 'insert', [Int, Var, List], List, c.list_insert)

  return scope
