"""Contains the Token class."""
from dataclasses import dataclass
from app.token.token_type import TokenType

@dataclass(frozen=True)
class Token:
  """Represents a lexing token pulled from source."""
  type: TokenType
  value: str
  source_line: int
  source_column: int

  @property
  def length(self) -> int:
    """Returns the length of the value of the instance."""
    return len(self.value)
