# pylint: disable=too-many-public-methods, missing-docstring, invalid-name
from unittest import TestCase
from app.scope import Scope
from app.scope.key import (
  TypeKey,
  TypeVariableKey,
  ContainerTypeKey,
  ScopeKey,
  FunctionScopeKey,
)

class ScopeTest(TestCase):
  def test_has_function(self):
    scope = Scope()

    key1 = FunctionScopeKey('add', [TypeKey('Int'), TypeKey('Int')], TypeKey('Int'))
    key2 = FunctionScopeKey('add', [TypeKey('Real'), TypeKey('Real')], TypeKey('Real'))
    fn = object()

    scope.add(key1, fn)

    self.assertTrue(scope.has_function(key1))
    self.assertFalse(scope.has_function(key2))

  def test_parent_has_function(self):
    scope1 = Scope()
    scope2 = Scope(scope1)

    key1 = FunctionScopeKey('add', [TypeKey('Int'), TypeKey('Int')], TypeKey('Int'))
    key2 = FunctionScopeKey('add', [TypeKey('Real'), TypeKey('Real')], TypeKey('Real'))
    fn = object()

    scope1.add(key1, fn)

    self.assertTrue(scope2.has_function(key1))
    self.assertFalse(scope2.has_function(key2))

  def test_has_value(self):
    scope = Scope()

    key1 = ScopeKey('x')
    key2 = ScopeKey('y')
    val = object()

    scope.add(key1, val)

    self.assertTrue(scope.has_value(key1))
    self.assertFalse(scope.has_value(key2))

  def test_parent_has_value(self):
    scope1 = Scope()
    scope2 = Scope(scope1)

    key1 = ScopeKey('x')
    key2 = ScopeKey('y')
    val = object()

    scope1.add(key1, val)

    self.assertTrue(scope2.has_value(key1))
    self.assertFalse(scope2.has_value(key2))

  def test_get_function(self):
    scope = Scope()

    key = FunctionScopeKey('add', [TypeKey('Int'), TypeKey('Int')], TypeKey('Int'))
    fn = object()

    scope.add(key, fn)

    self.assertEqual(scope.get_function(key), fn)

  def test_get_parent_function(self):
    scope1 = Scope()
    scope2 = Scope(scope1)

    key = FunctionScopeKey('add', [TypeKey('Int'), TypeKey('Int')], TypeKey('Int'))
    fn = object()

    scope1.add(key, fn)

    self.assertEqual(scope2.get_function(key), fn)

  def test_get_value(self):
    scope = Scope()

    key = ScopeKey('x')
    val = object()

    scope.add(key, val)

    self.assertEqual(scope.get_value(key), val)

  def test_get_parent_value(self):
    scope1 = Scope()
    scope2 = Scope(scope1)

    key = ScopeKey('x')
    val = object()

    scope1.add(key, val)

    self.assertEqual(scope2.get_value(key), val)

  def test_resolve_function_1(self):
    scope = Scope()

    key1 = FunctionScopeKey('add', [TypeKey('Int'), TypeKey('Int')], TypeKey('Int'))
    key2 = FunctionScopeKey('add', [TypeKey('Real'), TypeKey('Real')], TypeKey('Real'))
    fn1 = object()
    fn2 = object()

    scope.add(key1, fn1)
    scope.add(key2, fn2)

    res = scope.resolve_function(key1)

    self.assertEqual(len(res), 1)
    self.assertEqual(res[0].matched_key, key1)

  def test_resolve_parent_function_1(self):
    scope1 = Scope()
    scope2 = Scope(scope1)

    key1 = FunctionScopeKey('add', [TypeKey('Int'), TypeKey('Int')], TypeKey('Int'))
    key2 = FunctionScopeKey('add', [TypeKey('Real'), TypeKey('Real')], TypeKey('Real'))
    fn1 = object()
    fn2 = object()

    scope1.add(key1, fn1)
    scope1.add(key2, fn2)

    res = scope2.resolve_function(key1)

    self.assertEqual(len(res), 1)
    self.assertEqual(res[0].matched_key, key1)

  def test_resolve_function_2(self):
    scope = Scope()

    key1 = FunctionScopeKey('add', [TypeKey('Int'), TypeKey('Int')], TypeKey('Int'))
    key2 = FunctionScopeKey('add', [TypeKey('Int'), TypeKey('Int')])
    fn1 = object()

    scope.add(key1, fn1)

    res = scope.resolve_function(key2)

    self.assertEqual(len(res), 1)
    self.assertEqual(res[0].matched_key, key1)

  def test_resolve_function_3(self):
    scope = Scope()

    key = FunctionScopeKey(
      'head',
      [ContainerTypeKey('List', [TypeVariableKey('A')])],
      TypeVariableKey('A'),
    )
    fn = object()

    scope.add(key, fn)

    input_key = FunctionScopeKey(
      'head',
      [ContainerTypeKey('List', [TypeKey('Int')])],
    )
    resolved_key = FunctionScopeKey(
      'head',
      [ContainerTypeKey('List', [TypeKey('Int')])],
      TypeKey('Int'),
    )

    res = scope.resolve_function(input_key)

    self.assertEqual(len(res), 1)
    self.assertEqual(res[0].matched_key, key)
    self.assertEqual(res[0].resolved_key, resolved_key)

  def test_resolve_function_4(self):
    scope = Scope()

    key = FunctionScopeKey(
      'map',
      [
        ContainerTypeKey('Function', [TypeVariableKey('A'), TypeVariableKey('B')]),
        ContainerTypeKey('List', [TypeVariableKey('A')]),
      ],
      ContainerTypeKey('List', [TypeVariableKey('B')]),
    )
    fn = object()

    scope.add(key, fn)

    input_key = FunctionScopeKey(
      'map',
      [
        ContainerTypeKey('Function', [TypeVariableKey('A'), TypeVariableKey('B')]),
        ContainerTypeKey('List', [TypeKey('Int')])
      ],
    )
    resolved_key = FunctionScopeKey(
      'map',
      [
        ContainerTypeKey('Function', [TypeKey('Int'), TypeVariableKey('B')]),
        ContainerTypeKey('List', [TypeKey('Int')]),
      ],
      ContainerTypeKey('List', [TypeVariableKey('B')]),
    )

    res = scope.resolve_function(input_key)

    self.assertEqual(len(res), 1)
    self.assertEqual(res[0].matched_key, key)
    self.assertEqual(res[0].resolved_key, resolved_key)
