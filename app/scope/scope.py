"""Contains the scope class."""
from functools import reduce
from typing import (Any, List, MutableMapping, Optional, Tuple)
from .key import (FunctionScopeKey, ScopeKey)

Function = Any
Value = Any
FunctionEntry = Tuple[FunctionScopeKey, Function]

class FunctionResolution:
  """Contains information about function resolution."""
  def __init__(self, input_key: FunctionScopeKey, matched_key: FunctionScopeKey) -> None:
    self.input_key: FunctionScopeKey = input_key
    self.matched_key: FunctionScopeKey = matched_key

    resolved_key = matched_key
    while True:
      resolutions = resolved_key.get_resolutions(input_key)
      if resolutions:
        resolved_key = reduce(
          lambda key, resolution: key.resolve(resolution),
          resolutions,
          resolved_key,
        )
      else:
        break

    self.resolved_key = resolved_key

  @property
  def is_match(self):
    return (
      self.input_key == self.matched_key
      or (
        self.input_key.name == self.matched_key.name
        and self.input_key.arg_types == self.matched_key.arg_types
        and self.input_key.is_unknown_return_type
      )
      or self.resolved_key != self.matched_key
    )

class Scope:
  """Contains a scope of values and functions."""
  def __init__(self, parent_scope: Optional['Scope'] = None) -> None:
    self._parent_scope = parent_scope
    self._values: MutableMapping[ScopeKey, Value] = {}
    self._functions: MutableMapping[str, List[FunctionEntry]] = {}

  def extend(self) -> 'Scope':
    return Scope(self)

  def add(self, key: ScopeKey, value: Value) -> None:
    if isinstance(key, FunctionScopeKey):
      self._add_function(key, value)
    else:
      self._add_value(key, value)

  def get_function(self, key: FunctionScopeKey) -> Function:
    fn = self._get_function(key)
    if fn is None:
      if self._parent_scope is not None:
        return self._parent_scope.get_function(key)
      raise KeyError(key)
    return fn

  def get_value(self, key: ScopeKey) -> Value:
    if self._has_own_value(key):
      return self._values[key]
    if self._parent_scope is not None:
      return self._parent_scope.get_value(key)
    raise KeyError(key)

  def has_function(self, key: FunctionScopeKey) -> bool:
    if self._has_own_function(key):
      return True
    if self._parent_scope is not None:
      return self._parent_scope.has_function(key)
    return False

  def has_value(self, key: ScopeKey) -> bool:
    if self._has_own_value(key):
      return True
    if self._parent_scope is not None:
      return self._parent_scope.has_value(key)
    return False

  def resolve_function(self, key: FunctionScopeKey) -> List[FunctionResolution]:
    """Returns a list of all functions registered in the stack that match or partially match the
    given key.
    """
    if key.name in self._functions:
      fn_res = [FunctionResolution(key, fnkey) for (fnkey, _) in self._functions[key.name]]
    else:
      fn_res = []

    matched = [res for res in fn_res if res.is_match]

    if self._parent_scope:
      return matched + self._parent_scope.resolve_function(key)
    return matched

  def _add_function(self, key: FunctionScopeKey, value: Function) -> None:
    if key.is_unknown_return_type:
      raise Exception('function with unknown return type cannot be added to the scope')
    if self.has_function(key):
      raise Exception('function with key {0} already exists'.format(key))
    if key.name not in self._functions:
      self._functions[key.name] = []
    self._functions[key.name].append((key, value))

  def _add_value(self, key: ScopeKey, value: Value) -> None:
    if self.has_value(key):
      raise Exception('value with key {0} already exists'.format(key))
    self._values[key] = value

  def _get_function(self, key: FunctionScopeKey) -> Optional[Function]:
    if key.name in self._functions:
      for (fkey, fn) in self._functions[key.name]:
        if key == fkey:
          return fn
    return None

  def _has_own_value(self, key: ScopeKey) -> bool:
    return key in self._values

  def _has_own_function(self, key: FunctionScopeKey) -> bool:
    return self._get_function(key) is not None
