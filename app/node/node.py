"""Contains Node classes."""
# pylint: disable=missing-docstring, too-many-arguments
from typing import (Iterable, List as ListT, Optional, Sequence)
from decimal import (Decimal)
from app.token import (Token)
from app.scope.key import (TypeKey)

class Node:
  """Represents a parse token, comprising multiple lex tokens."""
  exclude_keys = {'index', 'length', 'resolved_type'}

  def __init__(self, index: int, length: int) -> None:
    self.index: int = index
    self.length: int = length
    self.resolved_type: Optional[TypeKey] = None

  def to_dict(self):
    raw_values = {
      k: getattr(self, k)
      for k in self.__dict__
      if k not in self.exclude_keys
    }
    values = {k: self.cook_value(v) for (k, v) in raw_values.items()}
    return {
      'type': self.__class__.__name__,
      'index': self.index,
      'length': self.length,
      'children': values,
    }

  @classmethod
  def cook_value(cls, value):
    if isinstance(value, Node):
      return value.to_dict()
    if isinstance(value, list):
      return [cls.cook_value(v) for v in value]
    return value

  def __repr__(self) -> str:
    return str(self.to_dict())

def sum_node_lengths(nodes: Iterable[Node]) -> int:
  return sum(node.length for node in nodes)

class Assignment(Node):
  def __init__(self, index: int, length: int, target: Node, value: Node) -> None:
    super().__init__(index, length)
    self.target: Node = target
    self.value: Node = value

class Block(Node):
  def __init__(self, index: int, length: int, assignments: ListT[Node], return_value: Node) -> None:
    super().__init__(index, length)
    self.assignments: ListT[Node] = assignments
    self.return_value: Node = return_value

class Boolean(Node):
  def __init__(self, index: int, value: str) -> None:
    super().__init__(index, 1)
    self.value: bool = value == 'true'

class Character(Node):
  def __init__(self, index: int, value: str) -> None:
    super().__init__(index, 1)
    self.value: str = value[1:-1]

class Discard(Node):
  pass

class FunctionCall(Node):
  def __init__(self, index: int, length: int, function: Node, args: ListT[Node]) -> None:
    super().__init__(index, length)
    self.function: Node = function
    self.arguments: ListT[Node] = args

class FunctionDefinition(Node):
  def __init__(
      self,
      index: int,
      length: int,
      type_vars: ListT[Node],
      args: ListT[Node],
      return_type: Optional[Node],
      body: Node
  ) -> None:
    super().__init__(index, length)
    self.type_variables: ListT[Node] = type_vars
    self.arguments: ListT[Node] = args
    self.return_type: Optional[Node] = return_type
    self.body: Node = body

class FunctionDefinitionArgument(Node):
  def __init__(self, index: int, length: int, arg_type: Node, name: Node) -> None:
    super().__init__(index, length)
    self.type: Node = arg_type
    self.name: Node = name

class FunctionType(Node):
  def __init__(self, index: int, length: int, args: Sequence[Node], return_type: Node) -> None:
    super().__init__(index, length)
    self.arguments: Sequence[Node] = args
    self.return_type: Node = return_type

class Identifier(Node):
  def __init__(self, index: int, name: str) -> None:
    super().__init__(index, 1)
    self.name: str = name

class Integer(Node):
  def __init__(self, index: int, value: str) -> None:
    super().__init__(index, 1)
    self.value: int = int(value)

class List(Node):
  def __init__(self, index: int, length: int, values: ListT[Node]) -> None:
    super().__init__(index, length)
    self.values: ListT[Node] = values

class ListType(Node):
  def __init__(self, index: int, length: int, member_type: Node) -> None:
    super().__init__(index, length)
    self.member_type: Node = member_type

class Placeholder(Node):
  def __init__(self, index: int, token: Optional[Token] = None) -> None:
    length = 0 if token is None else 1
    super().__init__(index, length)
    self.token: Optional[Token] = token

class Program(Node):
  def __init__(self, index: int, length: int, assignments: ListT[Node]) -> None:
    super().__init__(index, length)
    self.assignments: ListT[Node] = assignments

class Real(Node):
  def __init__(self, index: int, value: str) -> None:
    super().__init__(index, 1)
    self.value: Decimal = Decimal(value)

class String(Node):
  def __init__(self, index: int, value: str) -> None:
    super().__init__(index, 1)
    self.value: str = value[1:-1]

class Tuple(Node):
  def __init__(self, index: int, length: int, values: ListT[Node]) -> None:
    super().__init__(index, length)
    self.values: ListT[Node] = values

class TupleAssignment(Node):
  def __init__(self, index: int, length: int, targets: ListT[Node]) -> None:
    super().__init__(index, length)
    self.targets: ListT[Node] = targets

class TupleType(Node):
  def __init__(self, index: int, length: int, member_types: Sequence[Node]) -> None:
    super().__init__(index, length)
    self.member_types: Sequence[Node] = member_types

class TypeName(Node):
  def __init__(self, index: int, name: str) -> None:
    super().__init__(index, 1)
    self.name: str = name

class TypeVariables(Node):
  def __init__(self, index: int, length: int, type_names: ListT[Node]) -> None:
    super().__init__(index, length)
    self.type_names: ListT[Node] = type_names
