# pylint: disable=too-many-public-methods, missing-docstring, invalid-name
from unittest import TestCase
from app.scope.key import (
  ContainerTypeKey,
  FunctionScopeKey,
  ScopeKey,
  TypeKey,
  TypeResolution,
  TypeVariableKey,
  has_conflicting_resolutions,
)

class ScopeKeyTest(TestCase):
  # resolve
  def test_str(self):
    self.assertEqual(str(TypeKey('Int')), 'Int')
    self.assertEqual(str(TypeVariableKey('A')), 'A')
    self.assertEqual(
      str(ContainerTypeKey('Function', ['Int', 'Int', 'Real'])),
      'Function[Int,Int,Real]',
    )
    self.assertEqual(str(ScopeKey('x')), 'x')
    self.assertEqual(
      str(FunctionScopeKey('add', [TypeKey('Int'), TypeKey('Int')], TypeKey('Int'))),
      'add[Int,Int]=>Int',
    )

  def test_type_key_resolve(self):
    k = TypeKey('Int')
    self.assertFalse(k.is_variable_type)
    self.assertEqual(k.resolve(TypeResolution('A', TypeKey('Real'))), k)

  def test_type_variable_key_resolve_name_matches(self):
    vk = TypeVariableKey('A')
    self.assertTrue(vk.is_variable_type)
    k = vk.resolve(TypeResolution('A', TypeKey('Real')))
    self.assertEqual(k, TypeKey('Real'))

  def test_type_variable_key_resolve_name_does_not_match(self):
    vk = TypeVariableKey('A')
    k = vk.resolve(TypeResolution('B', TypeKey('Real')))
    self.assertEqual(k, TypeVariableKey('A'))
    self.assertNotEqual(k, TypeKey('Real'))

  def test_container_type_key_resolve_all_same(self):
    vck = ContainerTypeKey('Tuple', [TypeVariableKey('A'), TypeVariableKey('A')])
    self.assertTrue(vck.is_variable_type)
    ck = vck.resolve(TypeResolution('A', TypeKey('Real')))
    self.assertFalse(ck.is_variable_type)
    self.assertEqual(ck, ContainerTypeKey('Tuple', [TypeKey('Real'), TypeKey('Real')]))

  def test_container_type_key_resolve_different(self):
    vck1 = ContainerTypeKey(
      'Tuple',
      [TypeVariableKey('A'), TypeVariableKey('B'), TypeKey('Int')],
    )
    self.assertTrue(vck1.is_variable_type)

    vck2 = vck1.resolve(TypeResolution('C', TypeKey('Bool')))
    self.assertTrue(vck2.is_variable_type)
    self.assertEqual(vck2, vck1)

    vck3 = vck2.resolve(TypeResolution('B', TypeKey('Char')))
    self.assertTrue(vck3.is_variable_type)
    self.assertEqual(
      vck3,
      ContainerTypeKey(
        'Tuple',
        [TypeVariableKey('A'), TypeKey('Char'), TypeKey('Int')],
      ),
    )

    ck = vck3.resolve(TypeResolution('A', TypeKey('Bool')))
    self.assertFalse(ck.is_variable_type)
    self.assertEqual(
      ck,
      ContainerTypeKey(
        'Tuple',
        [TypeKey('Bool'), TypeKey('Char'), TypeKey('Int')],
      ),
    )

  def test_container_type_key_resolve_nested_containers(self):
    vck1 = ContainerTypeKey(
      'Tuple',
      [
        TypeVariableKey('A'),
        ContainerTypeKey(
          'List',
          [TypeVariableKey('B')],
        ),
      ],
    )

    self.assertTrue(vck1.is_variable_type)

    vck2 = vck1.resolve(TypeResolution('A', TypeKey('Int')))
    self.assertTrue(vck2.is_variable_type)

    vck3 = vck2.resolve(TypeResolution('B', TypeKey('Real')))
    self.assertFalse(vck3.is_variable_type)
    self.assertEqual(
      vck3,
      ContainerTypeKey(
        'Tuple',
        [
          TypeKey('Int'),
          ContainerTypeKey('List', [TypeKey('Real')]),
        ],
      ),
    )

  def test_function_scope_key_resolve(self):
    vfk1 = FunctionScopeKey(
      'zip',
      [
        ContainerTypeKey('List', [TypeVariableKey('A')]),
        ContainerTypeKey('List', [TypeVariableKey('B')]),
      ],
      ContainerTypeKey('Tuple', [TypeVariableKey('A'), TypeVariableKey('B')]),
    )
    self.assertTrue(vfk1.is_variable_type)

    vfk2 = vfk1.resolve(TypeResolution('B', TypeKey('Real')))
    self.assertTrue(vfk2.is_variable_type)

    vfk3 = vfk2.resolve(TypeResolution('A', TypeKey('Int')))
    self.assertFalse(vfk3.is_variable_type)
    self.assertEqual(
      vfk3,
      FunctionScopeKey(
        'zip',
        [
          ContainerTypeKey('List', [TypeKey('Int')]),
          ContainerTypeKey('List', [TypeKey('Real')]),
        ],
        ContainerTypeKey('Tuple', [TypeKey('Int'), TypeKey('Real')]),
      )
    )

  # get_resolutions
  def test_type_key_get_resolutions_with_type_key(self):
    tk1 = TypeKey('Int')
    tk2 = TypeKey('Real')
    self.assertEqual(tk1.get_resolutions(tk2), [])

  def test_type_variable_key_get_resolutions_with_type_key(self):
    tvk = TypeVariableKey('A')
    tk = TypeKey('Int')
    self.assertEqual(tvk.get_resolutions(tk), [TypeResolution('A', TypeKey('Int'))])

  def test_type_variable_key_get_resolutions_with_type_variable_key(self):
    tvk1 = TypeVariableKey('A')
    tvk2 = TypeVariableKey('B')
    self.assertEqual(tvk1.get_resolutions(tvk2), [])

  def test_container_type_key_get_resolutions_single_variable(self):
    ctk1 = ContainerTypeKey('List', [TypeVariableKey('A')])
    ctk2 = ContainerTypeKey('List', [TypeKey('Char')])
    self.assertEqual(ctk1.get_resolutions(ctk2), [TypeResolution('A', TypeKey('Char'))])

  def test_container_type_key_get_resolutions_multiple_variables(self):
    ctk1 = ContainerTypeKey(
      'Tuple',
      [
        TypeVariableKey('A'),
        TypeVariableKey('B'),
      ],
    )
    ctk2 = ContainerTypeKey(
      'Tuple',
      [
        ContainerTypeKey('List', [TypeKey('Int')]),
        TypeKey('Char'),
      ],
    )
    self.assertEqual(
      ctk1.get_resolutions(ctk2),
      [
        TypeResolution('A', ContainerTypeKey('List', [TypeKey('Int')])),
        TypeResolution('B', TypeKey('Char')),
      ],
    )

  def test_container_type_key_get_resolutions_multiple_variables_mixed_types(self):
    # NOTE: resolutions will blindly return all resolutions, even if there are conflicts. It is
    #       expected these will be handled at a higher level
    ctk1 = ContainerTypeKey(
      'Tuple',
      [
        TypeVariableKey('A'),
        TypeVariableKey('A'),
      ],
    )
    ctk2 = ContainerTypeKey('Tuple', [TypeKey('Int'), TypeKey('Real')])
    self.assertEqual(
      ctk1.get_resolutions(ctk2),
      [
        TypeResolution('A', TypeKey('Int')),
        TypeResolution('A', TypeKey('Real')),
      ],
    )

  def test_function_scope_key_get_resolutions(self):
    fk1 = FunctionScopeKey(
      'compose',
      [
        ContainerTypeKey('Function', [TypeVariableKey('B'), TypeVariableKey('C')]),
        ContainerTypeKey('Function', [TypeVariableKey('A'), TypeVariableKey('B')]),
      ],
      TypeVariableKey('C'),
    )
    fk2 = FunctionScopeKey(
      'compose',
      [
        ContainerTypeKey('Function', [TypeKey('Int'), TypeKey('Char')]),
        ContainerTypeKey('Function', [TypeKey('Real'), TypeKey('Int')]),
      ],
      TypeKey('Char'),
    )
    res = fk1.get_resolutions(fk2)
    self.assertEqual(
      res,
      [
        TypeResolution('B', TypeKey('Int')),
        TypeResolution('C', TypeKey('Char')),
        TypeResolution('A', TypeKey('Real')),
        TypeResolution('B', TypeKey('Int')),
      ],
    )

  # resolution conflicts
  def test_type_resolution_conflicts_with(self):
    tr1 = TypeResolution('A', 'Int')
    tr2 = TypeResolution('A', 'Int')
    tr3 = TypeResolution('B', 'Int')
    tr4 = TypeResolution('A', 'Real')

    self.assertFalse(tr1.conflicts_with(tr2))
    self.assertFalse(tr1.conflicts_with(tr3))
    self.assertTrue(tr1.conflicts_with(tr4))

  # has_conflicting_resolutions
  def test_has_conflicting_resolutions_no_conflicts(self):
    resolutions = [
      TypeResolution('A', TypeKey('Int')),
      TypeResolution('B', TypeKey('Char')),
      TypeResolution('C', TypeKey('Bool')),
      TypeResolution('A', TypeKey('Int')),
    ]
    self.assertFalse(has_conflicting_resolutions(resolutions))

  def test_has_conflicting_resolutions_with_conflicts(self):
    resolutions = [
      TypeResolution('A', TypeKey('Int')),
      TypeResolution('B', TypeKey('Char')),
      TypeResolution('A', TypeKey('Bool')),
      TypeResolution('C', TypeKey('Int')),
    ]
    self.assertTrue(has_conflicting_resolutions(resolutions))
