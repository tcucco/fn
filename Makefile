clean:
	rm -rf build/ dist/ .coverage .mypy_cache *.egg-info
	find . -iname __pycache__ -delete
	find . -iname *.pyc -delete
	find . -iname .DS_Store -delete

coverage:
	coverage run test.py
	coverage report

install:
	pip install --upgrade pip setuptools
	pip install -r requirements.txt

test:
	python test.py
