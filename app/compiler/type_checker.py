"""Contains methods for checking types of compiled programs."""
import typing as T
from app.node.node import (
  Boolean,
  Character,
  Integer,
  List,
  Node,
  Program,
  Real,
  String,
  Tuple,
)
from app.scope.key import (
  TypeKey,
  ContainerTypeKey,
)
from app.scope.scope import Scope
from app.lib.types import (
  int_type,
  real_type,
  bool_type,
  char_type,
  list_type,
  tuple_type,
)

class TypeCheckError(Exception):
  def __init__(self, node: Node, message: str) -> None:
    super().__init__()
    self.node = node
    self.message = message

def check_program(scope: Scope, program: Program) -> Program:
  return T.cast(Program, check(scope, program))

def check(scope: Scope, node: Node) -> Node:
  """Checks the type of nodes."""
  if isinstance(node, Integer):
    node.resolved_type = TypeKey(int_type)
  elif isinstance(node, Real):
    node.resolved_type = TypeKey(real_type)
  elif isinstance(node, Character):
    node.resolved_type = TypeKey(char_type)
  elif isinstance(node, Boolean):
    node.resolved_type = TypeKey(bool_type)
  elif isinstance(node, String):
    node.resolved_type = ContainerTypeKey(list_type, [TypeKey(char_type)])
  elif isinstance(node, List):
    return _check_list(scope, node)
  elif isinstance(node, Tuple):
    node.resolved_type = ContainerTypeKey(
      tuple_type,
      T.cast(T.List[TypeKey], [check(scope, v).resolved_type for v in node.values]),
    )
  return node

def _check_list(scope: Scope, node: List) -> List:
  types = {check(scope, v).resolved_type for v in node.values}
  if not types:
    raise TypeCheckError(node, 'Cannot determine type of empty list')
  if len(types) > 1:
    raise TypeCheckError(node, 'List elements must all be the same type')
  element_type = T.cast(TypeKey, types.pop())
  node.resolved_type = ContainerTypeKey(list_type, [element_type])
  return node
