# pylint: disable=too-many-public-methods, missing-docstring, invalid-name
from decimal import Decimal
from unittest import TestCase
from app.lib.core_scope import (
  Int,
  Real,
  Bool,
  Char,
  Pred,
  build_core_scope
)
from app.scope.key import (
  FunctionScopeKey as FK,
  ContainerTypeKey as CK
)

IntThunk = CK('Function', [Int])
IntList = CK('List', [Int])
CharList = CK('List', [Char])
core_scope = build_core_scope()

def get_function(key):
  resolved_key = core_scope.resolve_function(key)[0].matched_key
  return core_scope.get_function(resolved_key)

class CoreScopeTest(TestCase):
  def assert_result(self, name, arg_types, args, expected):
    fn = get_function(FK(name, arg_types))
    self.assertEqual(fn(*args), expected)

  def test_int_add(self):
    self.assert_result('add', [Int, Int], (1, 2), 3)

  def test_int_subtract(self):
    self.assert_result('sub', [Int, Int], (42, 13), 29)

  def test_int_multiply(self):
    self.assert_result('mul', [Int, Int], (3, 4), 12)

  def test_int_divide(self):
    self.assert_result('div', [Int, Int], (12, 5), 2)

  def test_int_less_than(self):
    self.assert_result('lt', [Int, Int], (3, 4), True)
    self.assert_result('lt', [Int, Int], (4, 4), False)
    self.assert_result('lt', [Int, Int], (5, 4), False)

  def test_int_equal(self):
    self.assert_result('eq', [Int, Int], (3, 4), False)
    self.assert_result('eq', [Int, Int], (4, 4), True)

  def test_real_add(self):
    self.assert_result('add', [Real, Real], (Decimal('1'), Decimal('2')), Decimal('3'))

  def test_real_subtract(self):
    self.assert_result('sub', [Real, Real], (Decimal('42'), Decimal('13')), Decimal('29'))

  def test_real_multiply(self):
    self.assert_result('mul', [Real, Real], (Decimal('3'), Decimal('4')), Decimal('12'))

  def test_real_divide(self):
    self.assert_result('div', [Real, Real], (Decimal('12'), Decimal('5')), Decimal('2.4'))

  def test_real_less_than(self):
    self.assert_result('lt', [Real, Real], (Decimal('3'), Decimal('4')), True)
    self.assert_result('lt', [Real, Real], (Decimal('4'), Decimal('4')), False)
    self.assert_result('lt', [Real, Real], (Decimal('5'), Decimal('4')), False)

  def test_real_equal(self):
    self.assert_result('eq', [Real, Real], (Decimal('3'), Decimal('4')), False)
    self.assert_result('eq', [Real, Real], (Decimal('4'), Decimal('4')), True)

  def test_bool_equal(self):
    self.assert_result('eq', [Bool, Bool], (False, False), True)
    self.assert_result('eq', [Bool, Bool], (False, True), False)
    self.assert_result('eq', [Bool, Bool], (True, False), False)
    self.assert_result('eq', [Bool, Bool], (True, True), True)

  def test_bool_or(self):
    self.assert_result('or', [Bool, Bool], (True, False), True)
    self.assert_result('or', [Bool, Bool], (False, False), False)

  def test_bool_or_fn(self):
    self.assert_result('or', [Pred, Pred], (lambda: True, lambda: False), True)
    self.assert_result('or', [Pred, Pred], (lambda: False, lambda: False), False)

  def test_bool_and(self):
    self.assert_result('and', [Bool, Bool], (True, False), False)
    self.assert_result('and', [Bool, Bool], (True, True), True)

  def test_bool_and_fn(self):
    self.assert_result('and', [Pred, Pred], (lambda: True, lambda: False), False)
    self.assert_result('and', [Pred, Pred], (lambda: True, lambda: True), True)

  def test_bool_if(self):
    self.assert_result('if', [Bool, Int, Int], (True, 1, 2), 1)
    self.assert_result('if', [Bool, Int, Int], (False, 1, 2), 2)

  def test_bool_if_fn(self):
    self.assert_result('iff', [Bool, IntThunk, IntThunk], (True, lambda: 1, lambda: 2), 1)
    self.assert_result('iff', [Bool, IntThunk, IntThunk], (False, lambda: 1, lambda: 2), 2)

  def test_char_lt(self):
    self.assert_result('lt', [Char, Char], ('a', 'b'), True)
    self.assert_result('lt', [Char, Char], ('b', 'b'), False)
    self.assert_result('lt', [Char, Char], ('b', 'a'), False)

  def test_char_eq(self):
    self.assert_result('eq', [Char, Char], ('a', 'b'), False)
    self.assert_result('eq', [Char, Char], ('a', 'a'), True)

  def test_list_len(self):
    self.assert_result('len', [IntList], ([1, 2, 3], ), 3)

  def test_list_at(self):
    self.assert_result('at', [Int, IntList], (0, [1, 2, 3]), 1)
    self.assert_result('at', [Int, IntList], (1, [1, 2, 3]), 2)
    self.assert_result('at', [Int, IntList], (2, [1, 2, 3]), 3)

  def test_list_slice(self):
    self.assert_result('slice', [Int, Int, IntList], (0, 3, [1, 2, 3]), [1, 2, 3])
    self.assert_result('slice', [Int, Int, IntList], (0, 2, [1, 2, 3]), [1, 2])
    self.assert_result('slice', [Int, Int, IntList], (0, 1, [1, 2, 3]), [1])
    self.assert_result('slice', [Int, Int, IntList], (1, 3, [1, 2, 3]), [2, 3])
    self.assert_result('slice', [Int, Int, IntList], (2, 3, [1, 2, 3]), [3])
    self.assert_result('slice', [Int, Int, IntList], (1, 2, [1, 2, 3]), [2])

  def test_list_insert(self):
    cs = ['b', 'c', 'e']
    self.assert_result('insert', [Int, Char, CharList], (0, 'a', cs), ['a', 'b', 'c', 'e'])
    self.assert_result('insert', [Int, Char, CharList], (1, 'z', cs), ['b', 'z', 'c', 'e'])
    self.assert_result('insert', [Int, Char, CharList], (2, 'd', cs), ['b', 'c', 'd', 'e'])
    self.assert_result('insert', [Int, Char, CharList], (3, 'f', cs), ['b', 'c', 'e', 'f'])
