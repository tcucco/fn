"""Methods for matching Nodes from a TokenList."""
from typing import (Callable, List, Optional, Type, Union, cast)

from app.token import (TokenList, TokenType)
from app.token.token_list import (list_delta, merge)
import app.node.node as N
from .generic_node_matcher import (
  MatcherResult,
  concat_matchers,
  filter_result,
  one_or_more,
  optional,
  or_matcher,
  token_type,
  zero_or_more,
)

_boolean_literals = {'true', 'false'}
_reserved_words = {'_'} | _boolean_literals

def _get_node_class(node: Union[N.Node, Type[N.Node]]) -> str:
  if isinstance(node, N.Node):
    return node.__class__.__name__
  return node.__name__

def _success(token_list: TokenList, sub_list: TokenList, build_node: Callable[[int, int], N.Node]) -> MatcherResult:
  """Update token_list with the successful match and return a single result of a Node."""
  index = token_list.index
  delta = list_delta(token_list, sub_list)

  node = build_node(index, delta)
  sub_list.set_success(index, _get_node_class(node))

  merge(token_list, sub_list)
  return MatcherResult.single_match(node)

def _failure(token_list: TokenList, sub_list: TokenList, cls: Type[N.Node]) -> MatcherResult:
  """Update token_list failure and return a no match."""
  (fidx, fctx) = sub_list.failure_mark
  index = fidx if fidx >= 0 else sub_list.index
  context = fctx if fctx is not None else _get_node_class(cls)
  token_list.set_failure(index, context)
  return MatcherResult.no_match()

def match_assignment(token_list: TokenList) -> MatcherResult:
  """Match an Assignment node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  sub_list = token_list.copy()
  result = _filter_placeholders(_assignment_matcher(sub_list))

  if not result.success:
    return _failure(token_list, sub_list, N.Assignment)

  (target, value) = result.nodes
  return _success(
    token_list,
    sub_list,
    lambda index, delta: N.Assignment(index, delta, target, value),
  )

def match_block(token_list: TokenList) -> MatcherResult:
  """Match a Block node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  sub_list = token_list.copy()
  result = _filter_placeholders(_block_matcher(sub_list))

  if not result.success:
    return _failure(token_list, sub_list, N.Block)

  (*assignments, return_value) = result.nodes
  return _success(
    token_list,
    sub_list,
    lambda index, delta: N.Block(index, delta, assignments, return_value),
  )

def match_boolean(token_list: TokenList) -> MatcherResult:
  """Match a Boolean node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  token = token_list.next

  if token.type == TokenType.IDENTIFIER and token.value in _boolean_literals:
    node = N.Boolean(token_list.index, token.value)
    token_list.advance()
    return MatcherResult.single_match(node)

  return MatcherResult.no_match()

match_character = token_type(TokenType.CHARACTER, N.Character)

def match_discard(token_list: TokenList) -> MatcherResult:
  """Match an Discard node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  token = token_list.next

  if token.type == TokenType.IDENTIFIER and token.value == '_':
    node = N.Discard(token_list.index, 1)
    token_list.advance()
    return MatcherResult.single_match(node)

  return MatcherResult.no_match()

def match_function_call(token_list: TokenList) -> MatcherResult:
  """Match a FunctionCall node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  sub_list = token_list.copy()
  result = _filter_placeholders(_function_call_matcher(sub_list))

  if not result.success:
    return _failure(token_list, sub_list, N.FunctionCall)

  (function_name, *args) = result.nodes
  return _success(
    token_list,
    sub_list,
    lambda index, delta: N.FunctionCall(index, delta, function_name, args),
  )

def match_function_definition(token_list: TokenList) -> MatcherResult:
  """Match a FunctionDefinition node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  sub_list = token_list.copy()
  type_vars_result = match_type_variables(sub_list)

  if type_vars_result.success:
    type_vars: List[N.Node] = cast(N.TypeVariables, type_vars_result.first).type_names
  else:
    type_vars = []

  arguments_result = match_function_definition_arguments(sub_list)

  if not arguments_result.success:
    return _failure(token_list, sub_list, N.FunctionDefinition)

  arrow_result = token_type(TokenType.ARROW)(sub_list)

  if not arrow_result.success:
    return _failure(token_list, sub_list, N.FunctionDefinition)

  return_type_result = _type_matcher(sub_list)

  if return_type_result.success:
    return_type: Optional[N.Node] = return_type_result.first
  else:
    return_type = None

  body_result = _value_matcher(sub_list)

  if not body_result.success:
    return _failure(token_list, sub_list, N.FunctionDefinition)

  body = body_result.first

  node = N.FunctionDefinition(
    token_list.index,
    list_delta(token_list, sub_list),
    type_vars,
    arguments_result.nodes,
    return_type,
    body,
  )
  merge(token_list, sub_list)
  return MatcherResult.single_match(node)

def match_function_definition_argument(token_list: TokenList) -> MatcherResult:
  """Match a FunctionDefinitionArgument node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  sub_list = token_list.copy()
  result = _filter_placeholders(_function_definition_argument_matcher(sub_list))

  if not result.success:
    return _failure(token_list, sub_list, N.FunctionDefinitionArgument)

  (arg_type, arg_name) = result.nodes
  return _success(
    token_list,
    sub_list,
    lambda index, delta: N.FunctionDefinitionArgument(index, delta, arg_type, arg_name),
  )

def match_function_definition_arguments(token_list: TokenList) -> MatcherResult:
  """Match function definitions."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  sub_list = token_list.copy()
  result = _filter_placeholders(_function_definition_arguments_matcher(sub_list))

  if not result.success:
    return MatcherResult.no_match()

  merge(token_list, sub_list)
  return result

def match_function_type(token_list: TokenList) -> MatcherResult:
  """Match a FunctionType node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  sub_list = token_list.copy()
  result = _filter_placeholders(function_type_matcher(sub_list))

  if not result.success:
    return _failure(token_list, sub_list, N.FunctionType)

  *args, return_type = result.nodes

  return _success(
    token_list,
    sub_list,
    lambda index, delta: N.FunctionType(index, delta, args, return_type),
  )

def match_identifier(token_list: TokenList) -> MatcherResult:
  """Match an Identifier node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  token = token_list.next

  if (token.type == TokenType.IDENTIFIER
      and (token.value[0].islower() or token.value[0] == '_')
      and token.value not in _reserved_words):
    node = N.Identifier(token_list.index, token.value)
    token_list.advance(node.length)
    return MatcherResult.single_match(node)

  return MatcherResult.no_match()

match_integer = token_type(TokenType.INTEGER, N.Integer)

def match_list(token_list: TokenList) -> MatcherResult:
  """Match a List node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  sub_list = token_list.copy()
  result = _filter_placeholders(_list_matcher(sub_list))

  if not result.success:
    return _failure(token_list, sub_list, N.List)

  return _success(token_list, sub_list, lambda index, delta: N.List(index, delta, result.nodes))

def match_list_type(token_list: TokenList) -> MatcherResult:
  """Match a ListType node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  sub_list = token_list.copy()
  result = _filter_placeholders(_list_type_matcher(sub_list))

  if not result.success:
    return _failure(token_list, sub_list, N.ListType)

  return _success(
    token_list,
    sub_list,
    lambda index, delta: N.ListType(index, delta, result.first),
  )

def match_program(token_list: TokenList) -> MatcherResult:
  """Match a Program node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  sub_list = token_list.copy()
  result = _filter_placeholders(_program_matcher(sub_list))

  if not result.success:
    return _failure(token_list, sub_list, N.Program)

  return _success(
    token_list,
    sub_list,
    lambda index, delta: N.Program(index, delta, result.nodes),
  )

match_real = token_type(TokenType.REAL_NUMBER, N.Real)

match_string = token_type(TokenType.STRING, N.String)

def match_tuple(token_list: TokenList) -> MatcherResult:
  """Match a Tuple node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  sub_list = token_list.copy()
  result = _filter_placeholders(_tuple_matcher(sub_list))

  if not result.success:
    return _failure(token_list, sub_list, N.Tuple)

  return _success(token_list, sub_list, lambda index, delta: N.Tuple(index, delta, result.nodes))

def match_tuple_assignment(token_list: TokenList) -> MatcherResult:
  """Match a TupleAssignment node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  sub_list = token_list.copy()
  result = _filter_placeholders(_tuple_assignment_matcher(sub_list))

  if not result.success:
    return _failure(token_list, sub_list, N.TupleAssignment)

  return _success(
    token_list,
    sub_list,
    lambda index, delta: N.TupleAssignment(index, delta, result.nodes),
  )

def match_tuple_type(token_list: TokenList) -> MatcherResult:
  """Match a TupleType node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  sub_list = token_list.copy()
  result = _filter_placeholders(_tuple_type_matcher(sub_list))

  if not result.success:
    return _failure(token_list, sub_list, N.TupleType)

  return _success(
    token_list,
    sub_list,
    lambda index, delta: N.TupleType(index, delta, result.nodes),
  )

def match_type_name(token_list: TokenList) -> MatcherResult:
  """Match a TypeName node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  token = token_list.next

  if token.type == TokenType.IDENTIFIER and token.value[0].isupper():
    node = N.TypeName(token_list.index, token.value)
    token_list.advance(node.length)
    return MatcherResult.single_match(node)

  return MatcherResult.no_match()

def match_type_variables(token_list: TokenList) -> MatcherResult:
  """Match a TypeVariables node."""
  if not token_list.has_more:
    return MatcherResult.no_match()

  sub_list = token_list.copy()
  result = _filter_placeholders(_type_variables_matcher(sub_list))

  if not result.success:
    return _failure(token_list, sub_list, N.TypeVariables)

  return _success(
    token_list,
    sub_list,
    lambda index, delta: N.TypeVariables(index, delta, result.nodes),
  )

def _filter_placeholders(result: MatcherResult) -> MatcherResult:
  return filter_result(lambda n: not isinstance(n, N.Placeholder), result)

_type_matcher = or_matcher([
  match_type_name,
  match_list_type,
  match_tuple_type,
  match_function_type,
])

_value_matcher = or_matcher([
  match_identifier,
  match_integer,
  match_real,
  match_character,
  match_boolean,
  match_string,
  match_list,
  match_tuple,
  match_block,
  match_function_definition,
  match_function_call,
])

_comma_matcher = token_type(TokenType.COMMA)

_one_or_more_values = concat_matchers([
  _value_matcher,
  zero_or_more(
    concat_matchers([
      _comma_matcher,
      _value_matcher,
    ]),
  ),
])

_two_or_more_values = concat_matchers([
  _value_matcher,
  _comma_matcher,
  _value_matcher,
  zero_or_more(
    concat_matchers([
      _comma_matcher,
      _value_matcher,
    ]),
  ),
])

_tuple_type_matcher = concat_matchers([
  token_type(TokenType.OPEN_PAREN),
  concat_matchers([
    _type_matcher,
    one_or_more(
      concat_matchers([
        _comma_matcher,
        _type_matcher,
      ]),
    ),
  ]),
  token_type(TokenType.CLOSE_PAREN),
])

_assignment_target_matcher = or_matcher([
  match_identifier,
  match_discard,
  match_tuple_assignment,
])

_assignment_matcher = concat_matchers([
  _assignment_target_matcher,
  token_type(TokenType.ASSIGNMENT),
  _value_matcher,
])

_block_matcher = concat_matchers([
  token_type(TokenType.OPEN_BRACE),
  zero_or_more(match_assignment),
  _value_matcher,
  token_type(TokenType.CLOSE_BRACE),
])

_function_call_matcher = concat_matchers([
  token_type(TokenType.OPEN_PAREN),
  or_matcher([match_identifier, match_function_definition]),
  zero_or_more(_value_matcher),
  token_type(TokenType.CLOSE_PAREN),
])

_function_definition_argument_matcher = concat_matchers([
  _type_matcher,
  _assignment_target_matcher,
])

_function_definition_arguments_matcher = concat_matchers([
  token_type(TokenType.OPEN_PAREN),
  optional(
    concat_matchers([
      match_function_definition_argument,
      zero_or_more(
        concat_matchers([
          _comma_matcher,
          match_function_definition_argument,
        ]),
      ),
    ])
  ),
  token_type(TokenType.CLOSE_PAREN),
])

function_type_matcher = concat_matchers([
  concat_matchers([
    token_type(TokenType.OPEN_PAREN),
    optional(
      concat_matchers([
        _type_matcher,
        zero_or_more(
          concat_matchers([
            _comma_matcher,
            _type_matcher,
          ]),
        ),
      ]),
    ),
    token_type(TokenType.CLOSE_PAREN),
  ]),
  token_type(TokenType.ARROW),
  _type_matcher,
])

_list_matcher = concat_matchers([
  token_type(TokenType.OPEN_BRACKET),
  _one_or_more_values,
  token_type(TokenType.CLOSE_BRACKET),
])

_list_type_matcher = concat_matchers([
  token_type(TokenType.OPEN_BRACKET),
  _type_matcher,
  token_type(TokenType.CLOSE_BRACKET),
])

_tuple_assignment_matcher = concat_matchers([
  token_type(TokenType.OPEN_PAREN),
  _assignment_target_matcher,
  zero_or_more(
    concat_matchers([
      _comma_matcher,
      _assignment_target_matcher,
    ]),
  ),
  token_type(TokenType.CLOSE_PAREN),
])

_tuple_matcher = concat_matchers([
  token_type(TokenType.OPEN_PAREN),
  _two_or_more_values,
  token_type(TokenType.CLOSE_PAREN),
])

_type_variables_matcher = concat_matchers([
  token_type(TokenType.OPEN_ANGLE_BRACKET),
  match_type_name,
  zero_or_more(
    concat_matchers([
      _comma_matcher,
      match_type_name,
    ]),
  ),
  token_type(TokenType.CLOSE_ANGLE_BRACKET),
])

_program_matcher = one_or_more(match_assignment)
