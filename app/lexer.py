"""Contains classes and methods for lexing source code."""
from typing import (Container, Iterable, Iterator)
from app.token import (Token, TokenMatch, TokenType, get_matches)

_default_ignore = frozenset([TokenType.WHITESPACE, TokenType.COMMENT])

def _compare_key(match: TokenMatch) -> int:
  """Compare two TokenMatchs. The token with the longest value will sort earlier.

  This means that if two matchers matched the same set of text, whichever one matched the most text
  will be sorted to the top, and therefore used.
  """
  return -len(match[1])

class Lexer:
  """Iteratively lexes source code."""
  def __init__(self, source: Iterable[str], ignore_tokens: Container[TokenType] = _default_ignore) -> None:
    self.source: Iterable[str] = source
    self.ignore_tokens: Container[TokenType] = ignore_tokens
    self.line_number: int = 0
    self.column_number: int = 0

  def process(self) -> Iterator[Token]:
    """Generate tokens from the source given the instance."""
    for (line_number, line) in enumerate(self.source):
      self.line_number = line_number
      yield from self._process_line(line)

  def _process_line(self, line: str) -> Iterator[Token]:
    remaining = line
    self.column_number = 0

    while remaining:
      token = self._next_token(remaining)
      remaining = remaining[token.length:]
      self.column_number += token.length
      if token.type not in self.ignore_tokens:
        yield token

  def _build_token(self, token_type: TokenType, value: str) -> Token:
    return Token(
      token_type,
      value,
      self.line_number,
      self.column_number,
    )

  def _next_token(self, line: str) -> Token:
    matches = list(get_matches(line))

    if not matches:
      raise Exception("Unable to match a token at line {0} column {1}: {2}".format(
        self.line_number + 1,
        self.column_number + 1,
        line
      ))

    if len(matches) == 1:
      (token_type, matched_value) = matches[0]
      return self._build_token(token_type, matched_value)

    (token_type, matched_value) = sorted(matches, key=_compare_key)[0]
    return self._build_token(token_type, matched_value)
