#! /bin/bash

set -o nounset
set -o errexit

ME=$(basename $0)
DIR=$(dirname $0)

DIRS="app tests"

PYLINT=false
MYPY=false
TEST=false

usage()
{
  cat <<EOF
NAME
  $ME

SYNOPSIS
  $ME -h
  $ME [-l] [-y] [-t] [-a]

DESCRIPTION
  $ME runs checkers on the project

OPTIONS
  -l    run the pylint linter

  -y    run the mypy type checker

  -t    run the test suite

  -a    equivalent to -lyt
EOF
}

while getopts "lytfah" OPTION; do
  case $OPTION in
    h) usage; exit ;;
    l) PYLINT=true ;;
    y) MYPY=true ;;
    t) TEST=true ;;
    a) PYLINT=true; MYPY=true; TEST=true ;;
    ?) usage; exit 1 ;;
  esac
done

if [ $PYLINT == true ]; then
  pipenv run pylint $DIRS
fi

if [ $MYPY == true ]; then
  pipenv run mypy $DIRS
fi

if [ $TEST == true ]; then
  pipenv run python -m tests
fi
