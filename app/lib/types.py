"""Core types."""
int_type = 'Int'
real_type = 'Real'
bool_type = 'Bool'
char_type = 'Char'
list_type = 'List'
tuple_type = 'Tuple'
function_type = 'Function'
