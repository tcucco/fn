# pylint: disable=missing-docstring
from unittest import TestCase
from typing import Iterable
from app.node.node import Program
from app.parser import Parser

def parse(source: Iterable[str]) -> Program:
  parser = Parser(source)
  return parser.process()

class ParserTest(TestCase):
  def test_process_success(self):
    source = [
      'x = 5',
      'y = 10',
      'z = (add x y)'
    ]
    program = parse(source)
    self.assertEqual(len(program.assignments), 3)

  def test_process_partial_failure(self):
    source = [
      'x = 5',
      'y = 10',
      '-- following function call is missing closing paren',
      'z = (add x y',
    ]
    with self.assertRaises(Exception) as cmgr:
      parse(source)
    self.assertEqual(str(cmgr.exception), '4:12: failed to parse FunctionCall')

  def test_process_total_failure(self):
    source = [
      '-- assignment is missing rhs',
      'x',
      'y = 10',
      'z = (add x y)',
    ]
    with self.assertRaises(Exception) as cmgr:
      parse(source)
    self.assertEqual(str(cmgr.exception), '3:1: failed to parse Assignment')
