# pylint: disable=missing-docstring
from decimal import Decimal
from json import dumps
from typing import (Any, Iterable)
from unittest import TestCase
from app.lib.core_scope import build_core_scope
from app.node.node import Program
from app.parser import Parser
from app.scope.key import ScopeKey
from app.scope.scope import Scope
from app.vm.vm import execute
from app.compiler.type_checker import check_program

def parse(source: Iterable[str]) -> Program:
  parser = Parser(source)
  return parser.process()

def compile_and_run(source: Iterable[str]):
  scope = build_core_scope()
  program = check_program(scope, parse(source))
  return execute(scope, program)

def debug_program(source: Iterable[str]):
  program = parse(source)
  print(dumps(program.to_dict(), indent=2, separators=(',', ': ')))

class VMTest(TestCase):
  def assert_scope_value(self, scope: Scope, key: str, value: Any) -> None:
    self.assertEqual(
      value,
      scope.get_value(ScopeKey(key)),
    )

  def assert_no_scope_value(self, scope: Scope, key: str) -> None:
    self.assertFalse(scope.has_value(ScopeKey(key)))

  def test_resolve_simple_assignments(self):
    source = [
      'a = 5',
      'b = 10.5',
      "c = 'a'",
      'd = true',
      'e = "hello, world"',
    ]
    scope = compile_and_run(source)
    self.assert_scope_value(scope, 'a', 5)
    self.assert_scope_value(scope, 'b', Decimal('10.5'))
    self.assert_scope_value(scope, 'c', 'a')
    self.assert_scope_value(scope, 'd', True)
    self.assert_scope_value(scope, 'e', 'hello, world')


  def test_resolve_container_assignments(self):
    source = [
      'a = [1, 2, 3, 4]',
      '''b = (1, 2.3, "trey", ('a', 'b'))''',
    ]
    scope = compile_and_run(source)
    self.assert_scope_value(scope, 'a', [1, 2, 3, 4])
    self.assert_scope_value(
      scope,
      'b',
      (
        1,
        Decimal('2.3'),
        'trey',
        ('a', 'b'),
      ),
    )

  def test_resolve_tuple_assignment(self):
    source = [
      '(a, _, _, b) = ("kevin", "jeff", "jay", "trey")',
    ]
    scope = compile_and_run(source)
    self.assert_scope_value(scope, 'a', 'kevin')
    self.assert_scope_value(scope, 'b', 'trey')
    self.assert_no_scope_value(scope, '_')

  def test_resolve_block(self):
    source = [
      'a = 10',
      '(b, c) = {',
      '  d = 5',
      '  (d, a)',
      '}',
    ]
    scope = compile_and_run(source)
    self.assert_scope_value(scope, 'b', 5)
    self.assert_scope_value(scope, 'c', 10)
    self.assert_no_scope_value(scope, 'd')

  def test_resolve_block_2(self):
    source = [
      'a = 1',
      '(b, c, d) = {',
      '  b = 2'
      '  {',
      '    c = 3',
      '    (a, b, c)',
      '  }',
      '}',
    ]
    scope = compile_and_run(source)
    self.assert_scope_value(scope, 'a', 1)
    self.assert_scope_value(scope, 'b', 1)
    self.assert_scope_value(scope, 'c', 2)
    self.assert_scope_value(scope, 'd', 3)
