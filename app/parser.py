"""Contains classes and methos for parsing source code."""
from typing import (Iterable, cast)
from app.lexer import Lexer
from app.node.node import Program
from app.node.node_matcher import match_program
from app.token import TokenList

class Parser:
  """Parse source code and report any errors found."""
  def __init__(self, source: Iterable[str]) -> None:
    self.source: Iterable[str] = source

  def process(self) -> Program:
    """Process source code into an AST."""
    lexer = Lexer(self.source)
    token_list = TokenList(list(lexer.process()))
    result = match_program(token_list)

    if not result.success or token_list.has_more:
      raise Exception(token_list.get_error_message())

    return cast(Program, result.first)
