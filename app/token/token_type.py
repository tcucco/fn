"""Contains an Enum of the types of lexing tokens."""
from enum import (Enum, auto)

class TokenType(Enum):
  """Enumeration of the types of tokens that can be found during lexing."""
  ARROW = auto()
  ASSIGNMENT = auto()
  CHARACTER = auto()
  CLOSE_ANGLE_BRACKET = auto()
  CLOSE_BRACE = auto()
  CLOSE_BRACKET = auto()
  CLOSE_PAREN = auto()
  COMMA = auto()
  COMMENT = auto()
  DOT = auto()
  IDENTIFIER = auto()
  INTEGER = auto()
  OPEN_ANGLE_BRACKET = auto()
  OPEN_BRACE = auto()
  OPEN_BRACKET = auto()
  OPEN_PAREN = auto()
  REAL_NUMBER = auto()
  STRING = auto()
  WHITESPACE = auto()
