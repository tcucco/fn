# pylint: disable=missing-docstring
from unittest import TestCase
from app.lexer import Lexer
from app.token.token_type import TokenType

class LexerTest(TestCase):
  def assert_tokens(self, expected, tokens):
    self.assertEqual(len(expected), len(tokens))

    for (exp, token) in zip(expected, tokens):
      self.assertEqual(
        exp,
        (token.type, token.value),
      )

  def assert_process(self, source, expected):
    lexer = Lexer(source, {TokenType.WHITESPACE})
    tokens = list(lexer.process())
    self.assert_tokens(expected, tokens)

  def test_process(self):
    source = [
      'Int x = 42',
      'Real y = 32.38',
      'Bool b = true',
      "Char c1 = 'a'",
      "Char c2 = '\\''",
      'Str s1 = "hello \\"world!\\""',
      "[Char] s2 = ['h', 'e', 'l', 'l', 'o']",
      '(Int, Real) p = (-3, -4.2)',
      'type Point { Real x, Real y }',
      'Real x = p.x',
      '(Int a, Int b) => { return (add a b) }',
      '<A, B>',
      "x' -- an integer value",
    ]
    expected = [
      (TokenType.IDENTIFIER, 'Int'),
      (TokenType.IDENTIFIER, 'x'),
      (TokenType.ASSIGNMENT, '='),
      (TokenType.INTEGER, '42'),

      (TokenType.IDENTIFIER, 'Real'),
      (TokenType.IDENTIFIER, 'y'),
      (TokenType.ASSIGNMENT, '='),
      (TokenType.REAL_NUMBER, '32.38'),

      (TokenType.IDENTIFIER, 'Bool'),
      (TokenType.IDENTIFIER, 'b'),
      (TokenType.ASSIGNMENT, '='),
      (TokenType.IDENTIFIER, 'true'),

      (TokenType.IDENTIFIER, 'Char'),
      (TokenType.IDENTIFIER, 'c1'),
      (TokenType.ASSIGNMENT, '='),
      (TokenType.CHARACTER, "'a'"),

      (TokenType.IDENTIFIER, 'Char'),
      (TokenType.IDENTIFIER, 'c2'),
      (TokenType.ASSIGNMENT, '='),
      (TokenType.CHARACTER, "'\\''"),

      (TokenType.IDENTIFIER, 'Str'),
      (TokenType.IDENTIFIER, 's1'),
      (TokenType.ASSIGNMENT, '='),
      (TokenType.STRING, '"hello \\"world!\\""'),

      (TokenType.OPEN_BRACKET, '['),
      (TokenType.IDENTIFIER, 'Char'),
      (TokenType.CLOSE_BRACKET, ']'),
      (TokenType.IDENTIFIER, 's2'),
      (TokenType.ASSIGNMENT, '='),
      (TokenType.OPEN_BRACKET, '['),
      (TokenType.CHARACTER, "'h'"),
      (TokenType.COMMA, ','),
      (TokenType.CHARACTER, "'e'"),
      (TokenType.COMMA, ','),
      (TokenType.CHARACTER, "'l'"),
      (TokenType.COMMA, ','),
      (TokenType.CHARACTER, "'l'"),
      (TokenType.COMMA, ','),
      (TokenType.CHARACTER, "'o'"),
      (TokenType.CLOSE_BRACKET, ']'),

      (TokenType.OPEN_PAREN, '('),
      (TokenType.IDENTIFIER, 'Int'),
      (TokenType.COMMA, ','),
      (TokenType.IDENTIFIER, 'Real'),
      (TokenType.CLOSE_PAREN, ')'),
      (TokenType.IDENTIFIER, 'p'),
      (TokenType.ASSIGNMENT, '='),
      (TokenType.OPEN_PAREN, '('),
      (TokenType.INTEGER, '-3'),
      (TokenType.COMMA, ','),
      (TokenType.REAL_NUMBER, '-4.2'),
      (TokenType.CLOSE_PAREN, ')'),

      (TokenType.IDENTIFIER, 'type'),
      (TokenType.IDENTIFIER, 'Point'),
      (TokenType.OPEN_BRACE, '{'),
      (TokenType.IDENTIFIER, 'Real'),
      (TokenType.IDENTIFIER, 'x'),
      (TokenType.COMMA, ','),
      (TokenType.IDENTIFIER, 'Real'),
      (TokenType.IDENTIFIER, 'y'),
      (TokenType.CLOSE_BRACE, '}'),

      (TokenType.IDENTIFIER, 'Real'),
      (TokenType.IDENTIFIER, 'x'),
      (TokenType.ASSIGNMENT, '='),
      (TokenType.IDENTIFIER, 'p'),
      (TokenType.DOT, '.'),
      (TokenType.IDENTIFIER, 'x'),

      (TokenType.OPEN_PAREN, '('),
      (TokenType.IDENTIFIER, 'Int'),
      (TokenType.IDENTIFIER, 'a'),
      (TokenType.COMMA, ','),
      (TokenType.IDENTIFIER, 'Int'),
      (TokenType.IDENTIFIER, 'b'),
      (TokenType.CLOSE_PAREN, ')'),
      (TokenType.ARROW, '=>'),
      (TokenType.OPEN_BRACE, '{'),
      (TokenType.IDENTIFIER, 'return'),
      (TokenType.OPEN_PAREN, '('),
      (TokenType.IDENTIFIER, 'add'),
      (TokenType.IDENTIFIER, 'a'),
      (TokenType.IDENTIFIER, 'b'),
      (TokenType.CLOSE_PAREN, ')'),
      (TokenType.CLOSE_BRACE, '}'),

      (TokenType.OPEN_ANGLE_BRACKET, '<'),
      (TokenType.IDENTIFIER, 'A'),
      (TokenType.COMMA, ','),
      (TokenType.IDENTIFIER, 'B'),
      (TokenType.CLOSE_ANGLE_BRACKET, '>'),

      (TokenType.IDENTIFIER, "x'"),
      (TokenType.COMMENT, '-- an integer value'),
    ]
    self.assert_process(source, expected)
