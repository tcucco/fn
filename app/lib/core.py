"""Core functions."""
# pylint: disable=invalid-name
from typing import Callable, List, TypeVar
from decimal import Decimal

U = TypeVar('U')
Pred = Callable[[], bool]
Thunk = Callable[[], U]

# integer
def int_add(l: int, r: int) -> int:
  return l + r

def int_subtract(l: int, r: int) -> int:
  return l - r

def int_multiply(l: int, r: int) -> int:
  return l * r

def int_divide(l: int, r: int) -> int:
  return l // r

def int_less_than(l: int, r: int) -> bool:
  return l < r

def int_equal(l: int, r: int) -> bool:
  return l == r

# real
def real_add(l: Decimal, r: Decimal) -> Decimal:
  return l + r

def real_subtract(l: Decimal, r: Decimal) -> Decimal:
  return l - r

def real_multiply(l: Decimal, r: Decimal) -> Decimal:
  return l * r

def real_divide(l: Decimal, r: Decimal) -> Decimal:
  return l / r

def real_less_than(l: Decimal, r: Decimal) -> bool:
  return l < r

def real_equal(l: Decimal, r: Decimal) -> bool:
  return l == r

# boolean
def bool_equal(l: bool, r: bool) -> bool:
  return l == r

def bool_or(l: bool, r: bool) -> bool:
  return l or r

def bool_or_fn(l: Pred, r: Pred) -> bool:
  if l():
    return True
  return r()

def bool_and(l: bool, r: bool) -> bool:
  return l and r

def bool_and_fn(l: Pred, r: Pred) -> bool:
  if l():
    return r()
  return False

def bool_if(cond: bool, if_true: U, if_false: U) -> U:
  if cond:
    return if_true
  return if_false

def bool_if_fn(cond: bool, if_true: Thunk, if_false: Thunk) -> U:
  if cond:
    return if_true()
  return if_false()

# character
def char_less_than(l: str, r: str) -> bool:
  return l < r

def char_equal(l: str, r: str) -> bool:
  return l == r

# list
def list_length(values: List[U]) -> int:
  return len(values)

def list_item(index: int, values: List[U]) -> U:
  return values[index]

def list_slice(start: int, end: int, values: List[U]) -> List[U]:
  return values[start:end]

def list_insert(index: int, value: U, values: List[U]) -> List[U]:
  new_list = list(values)
  new_list.insert(index, value)
  return new_list
