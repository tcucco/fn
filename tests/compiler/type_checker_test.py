# pylint: disable=missing-docstring
from unittest import TestCase
import app.node.node as N
import app.scope.key as K
from app.compiler.type_checker import TypeCheckError, check
from app.lib.core_scope import build_core_scope

class TypeCheckerTest(TestCase):
  def check(self, node: N.Node) -> N.Node:
    scope = build_core_scope()
    return check(scope, node)

  def assert_type_key(self, key: str, node: N.Node) -> None:
    self.assertEqual(
      K.TypeKey(key),
      node.resolved_type
    )

  def test_type_check_boolean(self):
    t = self.check(N.Boolean(0, 'true'))
    self.assert_type_key('Bool', t)

  def test_type_check_character(self):
    t = self.check(N.Character(0, 'A'))
    self.assert_type_key('Char', t)

  def test_type_check_integer(self):
    t = self.check(N.Integer(0, '42'))
    self.assert_type_key('Int', t)

  def test_type_check_real(self):
    t = self.check(N.Real(0, '42.42'))
    self.assert_type_key('Real', t)

  def test_type_check_string(self):
    t = self.check(N.String(0, '"Hellow, orld!"'))
    self.assertEqual(
      K.ContainerTypeKey('List', [K.TypeKey('Char')]),
      t.resolved_type
    )

  def test_type_check_list_success(self):
    t = self.check(N.List(0, 1, [N.Integer(0, '1'), N.Integer(0, '2'), N.Integer(0, '3')]))
    self.assertEqual(K.ContainerTypeKey('List', [K.TypeKey('Int')]), t.resolved_type)

  def test_type_check_list_empty_list(self):
    with self.assertRaises(TypeCheckError):
      self.check(N.List(0, 1, []))

  def test_type_check_list_different_element_types(self):
    with self.assertRaises(TypeCheckError):
      self.check(N.List(0, 1, [N.Real(0, '42.2'), N.Integer(0, '3')]))

  def test_type_check_tuple(self):
    t = self.check(N.Tuple(0, 1, [N.String(0, '"Greetings"'), N.Integer(0, '13'), N.Character(0, 'M')]))
    self.assertEqual(
      K.ContainerTypeKey(
        'Tuple',
        [
          K.ContainerTypeKey('List', [K.TypeKey('Char')]),
          K.TypeKey('Int'),
          K.TypeKey('Char'),
        ],
      ),
      t.resolved_type,
    )
