# pylint: disable=missing-docstring, wildcard-import
from tests.lexer import *
from tests.lib import *
from tests.node import *
from tests.parser import *
from tests.scope import *
from tests.vm import *
from tests.compiler import *
