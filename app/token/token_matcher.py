"""Contains methods for getting lexing tokens from source."""
import re
from typing import (Callable, Iterator, List, Optional, Pattern, Tuple)
from app.token.token_type import TokenType

Matcher = Callable[[str], Optional[str]]
TokenMatcher = Tuple[TokenType, Matcher]
TokenMatch = Tuple[TokenType, str]

_identifier_re = re.compile(r'''[a-zA-Z_][a-zA-Z0-9_]*'*''')
_whitespace_re = re.compile(r'''\s+''')
_integer_re = re.compile(r'''-?\d+''')
_real_number_re = re.compile(r'''-?\d+\.\d+''')
_char_re = re.compile(r"""'([^']|\\')'""")
_comment_re = re.compile(r'''--.*''')

def _if_match(regex: Pattern) -> Matcher:
  def _matcher(line: str) -> Optional[str]:
    match = regex.match(line)
    return match.group(0) if match else None
  return _matcher

def _if_literal(match: str) -> Matcher:
  def _matcher(line: str) -> Optional[str]:
    return match if line.startswith(match) else None
  return _matcher

def _or_match(check1: Matcher, check2: Matcher) -> Matcher:
  def _matcher(line: str) -> Optional[str]:
    return check1(line) or check2(line)
  return _matcher

_match_arrow = _if_literal('=>')
_match_assignment = _if_literal('=')
_match_character = _or_match(_if_literal("'\\''"), _if_match(_char_re))
_match_close_angle_bracket = _if_literal('>')
_match_close_brace = _if_literal('}')
_match_close_bracket = _if_literal(']')
_match_close_paren = _if_literal(')')
_match_comma = _if_literal(',')
_match_dot = _if_literal('.')
_match_identifier = _if_match(_identifier_re)
_match_integer = _if_match(_integer_re)
_match_open_angle_bracket = _if_literal('<')
_match_open_brace = _if_literal('{')
_match_open_bracket = _if_literal('[')
_match_open_paren = _if_literal('(')
_match_real_number = _if_match(_real_number_re)
_match_whitespace = _if_match(_whitespace_re)
_match_comment = _if_match(_comment_re)

def _match_string(line: str) -> Optional[str]:
  if line[0] != '"':
    return None

  is_escaped = False
  end_found = False
  last_index = 0

  for (index, char) in enumerate(line):
    if index == 0:
      continue

    if is_escaped:
      is_escaped = False
      continue

    if char == '"':
      end_found = True
      last_index = index
      break

    if char == '\\':
      is_escaped = True

  if not end_found:
    raise Exception('Unterminated string')

  return line[:last_index + 1]

_matchers: List[TokenMatcher] = [
  (TokenType.ARROW, _match_arrow),
  (TokenType.ASSIGNMENT, _match_assignment),
  (TokenType.CHARACTER, _match_character),
  (TokenType.CLOSE_ANGLE_BRACKET, _match_close_angle_bracket),
  (TokenType.CLOSE_BRACE, _match_close_brace),
  (TokenType.CLOSE_BRACKET, _match_close_bracket),
  (TokenType.CLOSE_PAREN, _match_close_paren),
  (TokenType.COMMA, _match_comma),
  (TokenType.DOT, _match_dot),
  (TokenType.IDENTIFIER, _match_identifier),
  (TokenType.INTEGER, _match_integer),
  (TokenType.OPEN_ANGLE_BRACKET, _match_open_angle_bracket),
  (TokenType.OPEN_BRACE, _match_open_brace),
  (TokenType.OPEN_BRACKET, _match_open_bracket),
  (TokenType.OPEN_PAREN, _match_open_paren),
  (TokenType.REAL_NUMBER, _match_real_number),
  (TokenType.STRING, _match_string),
  (TokenType.WHITESPACE, _match_whitespace),
  (TokenType.COMMENT, _match_comment),
]

def get_matches(line: str) -> Iterator[TokenMatch]:
  """Yields all tokens that match the beginning of a line of source."""
  for (token_type, matcher) in _matchers:
    matched_value = matcher(line)
    if matched_value:
      yield (token_type, matched_value)
