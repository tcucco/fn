"""Contains key types for scopes."""
from itertools import product
from typing import Sequence
from dataclasses import dataclass

@dataclass(frozen=True)
class TypeResolution:
  type_variable_name: str
  type_key: 'TypeKey'

  def conflicts_with(self, other: 'TypeResolution') -> bool:
    return self.type_variable_name == other.type_variable_name and self.type_key != other.type_key

@dataclass(frozen=True)
class TypeKey:
  """Concrete type key."""
  name: str

  @property
  def is_variable_type(self):
    return False

  def get_resolutions(self, _: 'TypeKey') -> Sequence[TypeResolution]: # pylint: disable=no-self-use
    return []

  def resolve(self, _: TypeResolution) -> 'TypeKey':
    return self

  def __str__(self):
    return self.name

UnknownType = TypeKey('Unknown')

class TypeVariableKey(TypeKey):
  """Type variable key."""
  @property
  def is_variable_type(self):
    return True

  def get_resolutions(self, type_key: TypeKey) -> Sequence[TypeResolution]:
    if not type_key.is_variable_type:
      return [TypeResolution(self.name, type_key)]
    return []

  def resolve(self, type_resolution: TypeResolution) -> TypeKey:
    if self.name == type_resolution.type_variable_name:
      return type_resolution.type_key
    return self

@dataclass(frozen=True)
class ContainerTypeKey(TypeKey):
  """Container type key. Is concrete if all types are concrete, variable otherwise."""
  types: Sequence[TypeKey]

  @property
  def is_variable_type(self):
    return any((t.is_variable_type for t in self.types))

  def get_resolutions(self, type_key: TypeKey) -> Sequence[TypeResolution]:
    if not isinstance(type_key, ContainerTypeKey):
      return []
    if type_key.name != self.name:
      return []
    if len(type_key.types) != len(self.types):
      return []
    return sum((l.get_resolutions(r) for (l, r) in zip(self.types, type_key.types)), [])

  def resolve(self, type_resolution: TypeResolution) -> TypeKey:
    if self.is_variable_type:
      return ContainerTypeKey(
        self.name,
        [t.resolve(type_resolution) for t in self.types],
      )
    return self

  def __str__(self):
    return '{0}[{1}]'.format(self.name, ','.join(str(t) for t in self.types))

@dataclass(frozen=True)
class ScopeKey:
  name: str

  def __str__(self):
    return self.name

@dataclass(frozen=True)
class FunctionScopeKey(ScopeKey):
  """Scope key for variables."""
  arg_types: Sequence[TypeKey]
  return_type: TypeKey = UnknownType

  @property
  def is_variable_type(self):
    return self.return_type.is_variable_type or any((t.is_variable_type for t in self.arg_types))

  @property
  def is_unknown_return_type(self):
    return self.return_type == UnknownType

  def resolve(self, type_resolution: TypeResolution) -> 'FunctionScopeKey':
    if self.is_variable_type:
      return FunctionScopeKey(
        self.name,
        [t.resolve(type_resolution) for t in self.arg_types],
        self.return_type.resolve(type_resolution),
      )
    return self

  def get_resolutions(self, function_key: 'FunctionScopeKey') -> Sequence[TypeResolution]:
    if function_key.name != self.name:
      return []
    return sum((l.get_resolutions(r) for (l, r) in zip(self.arg_types, function_key.arg_types)), [])

  def __str__(self):
    return "{0}[{1}]=>{2}".format(self.name, ','.join(str(a) for a in self.arg_types), self.return_type)

def has_conflicting_resolutions(resolutions: Sequence[TypeResolution]) -> bool:
  return any(l.conflicts_with(r) for (l, r) in product(resolutions, resolutions))
