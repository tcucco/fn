"""Contains methods for running programs."""
from decimal import Decimal
from typing import (Any, cast)
from app.node.node import (
  Node,
  Assignment,
  Block,
  Boolean,
  Character,
  Discard,
  # FunctionCall,
  # FunctionDefinition,
  # FunctionDefinitionArgument,
  # FunctionType,
  Identifier,
  Integer,
  List,
  Program,
  Real,
  String,
  Tuple,
  TupleAssignment,
)
from app.scope import (Scope)
from app.scope.key import (ScopeKey)

def execute(scope: Scope, program: Program) -> Program:
  return cast(Program, _resolve(scope, program))

def _resolve(scope: Scope, node: Any) -> Any:
  """Resolves a node."""
  # pylint: disable=too-many-return-statements
  if isinstance(node, (int, Decimal, str, bool, list, tuple)):
    return node
  if isinstance(node, (Integer, Real, Character, Boolean, String)):
    return node.value
  if isinstance(node, List):
    return [_resolve(scope, v) for v in node.values]
  if isinstance(node, Tuple):
    return tuple(_resolve(scope, v) for v in node.values)
  if isinstance(node, Identifier):
    return _resolve_identifier(scope, node)
  if isinstance(node, Program):
    return _resolve_program(scope, node)
  if isinstance(node, Block):
    return _resolve_block(scope, node)

  raise Exception('Unable to resolve node.', type(node))

def _resolve_program(outer_scope: Scope, node: Program) -> Scope:
  scope = outer_scope.extend()
  for assignment in node.assignments:
    _resolve_assignment(scope, cast(Assignment, assignment))
  return scope

def _add_to_scope(scope: Scope, target: Node, value: Any) -> None:
  if isinstance(target, Discard):
    return
  value = _resolve(scope, value)
  if isinstance(target, Identifier):
    scope.add(ScopeKey(target.name), value)
  elif isinstance(target, TupleAssignment):
    for (targ, val) in zip(target.targets, value):
      _add_to_scope(scope, targ, val)
  else:
    raise Exception('Unable to add to scope')

def _resolve_assignment(scope: Scope, node: Assignment) -> None:
  _add_to_scope(scope, node.target, node.value)

def _resolve_block(outer_scope: Scope, node: Block) -> Any:
  scope = outer_scope.extend()
  for assignment in node.assignments:
    _resolve_assignment(scope, cast(Assignment, assignment))
  return _resolve(scope, node.return_value)

def _resolve_identifier(scope: Scope, node: Identifier) -> Any:
  return scope.get_value(ScopeKey(node.name))
