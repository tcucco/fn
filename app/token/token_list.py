"""Contains a TokenList class."""
from typing import (Optional, Sequence, Tuple)
from app.token.token import Token

class TokenList:
  """Maintains a token list and position to simplify parsing."""
  def __init__(self, tokens: Sequence[Token]) -> None:
    self.tokens: Sequence[Token] = tokens
    self.index: int = 0
    self.success_mark: Tuple[int, Optional[str]] = (-1, None)
    self.failure_mark: Tuple[int, Optional[str]] = (-1, None)

  def __len__(self):
    return len(self.tokens)

  def advance(self, n: int = 1) -> 'TokenList':
    """Advance the index of the token list."""
    self.index += n
    return self

  def set_index(self, n: int) -> 'TokenList':
    """Set the index of the token list."""
    self.index = n
    return self

  def copy(self, advance: int = 0) -> 'TokenList':
    """Return a new TokenList with the index preset."""
    token_list = TokenList(self.tokens)
    token_list.advance(self.index + advance)
    return token_list

  def set_success(self, index: int, context: Optional[str]) -> 'TokenList':
    """Set the index and context of the most recent parsing failure."""
    (sidx, _) = self.success_mark
    if index > sidx:
      self.success_mark = (index, context)
    return self

  def set_failure(self, index: int, context: Optional[str]) -> 'TokenList':
    """Set the index and context of the most recent parsing failure."""
    (fidx, _) = self.failure_mark
    if index > fidx:
      self.failure_mark = (index, context)
    return self

  def token_at(self, index: int) -> Token:
    """Return the token an at index."""
    return self.tokens[index]

  def get_error_message(self):
    (fidx, fctx) = self.failure_mark

    if fidx == -1:
      return 'Unable to process source'

    ftok = self.token_at(fidx)

    return '{0}:{1}: failed to parse {2}'.format(
      ftok.source_line + 1,
      ftok.source_column + 1,
      fctx,
    )

  @property
  def next(self) -> Token:
    """Returns the next token in the remainder."""
    return self.tokens[self.index]

  @property
  def has_more(self) -> bool:
    """Returns whether or not there are any more tokens."""
    return len(self.tokens) > self.index

def list_delta(list_a: TokenList, list_b: TokenList) -> int:
  return abs(list_a.index - list_b.index)

def fast_forward(target: TokenList, source: TokenList) -> None:
  target.set_index(source.index)

def set_marks(target: TokenList, source: TokenList) -> None:
  (sidx, sctx) = source.success_mark
  target.set_success(sidx, sctx)

  (fidx, fctx) = source.failure_mark
  target.set_failure(fidx, fctx)

def set_failure(target: TokenList, source: TokenList) -> None:
  if source.index == target.index:
    return

  (fidx, fctx) = source.failure_mark

  if fidx == -1:
    target.set_failure(min(source.index, len(source) - 1), None)
  else:
    target.set_failure(fidx, fctx)

def merge(target: TokenList, source: TokenList) -> None:
  target.set_index(source.index)
  set_marks(target, source)
