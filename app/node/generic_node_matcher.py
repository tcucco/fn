"""Generic Node matchers."""
from typing import (Callable, List, Sequence, TypeVar)
from dataclasses import dataclass
from app.node.node import (Node, Placeholder)
from app.token import (TokenList, TokenType)
from app.token.token_list import merge, set_failure

@dataclass(frozen=True)
class MatcherResult:
  """Contains information about the success of a node match."""
  success: bool
  nodes: List[Node]

  @property
  def first(self) -> Node:
    return self.nodes[0]

  @staticmethod
  def no_match():
    return MatcherResult(False, [])

  @staticmethod
  def single_match(node: Node) -> 'MatcherResult':
    return MatcherResult(True, [node])

  @staticmethod
  def match(nodes: List[Node]) -> 'MatcherResult':
    return MatcherResult(True, nodes)

Matcher = Callable[[TokenList], MatcherResult]

U = TypeVar('U')
V = TypeVar('V')
W = TypeVar('W')

def compose(outer: Callable[[V], W], inner: Callable[[U], V]) -> Callable[[U], W]:
  def composed(val: U) -> W:
    return outer(inner(val))
  return composed

def filter_result(pred: Callable[[Node], bool], result: MatcherResult) -> MatcherResult:
  if result.success:
    return MatcherResult.match(list(filter(pred, result.nodes)))
  return result

def optional(sub_matcher: Matcher) -> Matcher:
  """Returns a matcher that will try to match a sub matcher against a TokenList one time, and will
  always return success even if the matcher didn't match.
  """
  def matcher(token_list: TokenList) -> MatcherResult:
    result = sub_matcher(token_list)
    return MatcherResult.match(result.nodes)
  return matcher

def one_or_more(sub_matcher: Matcher) -> Matcher:
  """Returns a Matcher that will continue to run a sub matcher against a TokenList so long as it
  returns results. The matcher will return success only if the sub matcher matched at least once.
  """
  def matcher(token_list: TokenList) -> MatcherResult:
    success = False
    nodes: List[Node] = []

    while True:
      result = sub_matcher(token_list)
      if result.success:
        nodes.extend(result.nodes)
        success = True
      else:
        break

    if not success:
      return MatcherResult.no_match()

    return MatcherResult.match(nodes)
  return matcher

zero_or_more = compose(optional, one_or_more)

def or_matcher(matchers: Sequence[Matcher]) -> Matcher:
  def matcher(token_list: TokenList) -> MatcherResult:
    for m in matchers:
      result = m(token_list)
      if result.success:
        return result
    return MatcherResult.no_match()
  return matcher

def concat_matchers(matchers: Sequence[Matcher]) -> Matcher:
  """Returns a matcher that will try to match a sequence of matchers against a TokenList. It will
  return success only if all matchers returned success.
  """
  def matcher(token_list: TokenList) -> MatcherResult:
    sub_list = token_list.copy()
    nodes: List[Node] = []

    for m in matchers:
      result = m(sub_list)
      if result.success:
        nodes.extend(result.nodes)
      else:
        set_failure(token_list, sub_list)
        return MatcherResult.no_match()

    merge(token_list, sub_list)
    return MatcherResult.match(nodes)
  return matcher

def token_type(ttype: TokenType, build_node: Callable[[int, str], Node] = None) -> Matcher:
  """Returns a matcher that matches a token type."""
  def matcher(token_list: TokenList) -> MatcherResult:
    if not token_list.has_more:
      return MatcherResult.no_match()
    token = token_list.next
    if token.type == ttype:
      node = build_node(token_list.index, token.value) if build_node else Placeholder(token_list.index, token)
      token_list.advance()
      return MatcherResult.single_match(node)
    return MatcherResult.no_match()
  return matcher
